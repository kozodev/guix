```
(cons* (channel
        (name 'kozoguix)
        (url "https://gitlab.com/kozodev/guix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
	 "979046c24405bf02be03d87aaa472b24137d4350"
          (openpgp-fingerprint
           "07F6 29B6 FD5F 68EC DF1F 0E50 598E F91C 8268 3C4C"))))
       %default-channels)
```
